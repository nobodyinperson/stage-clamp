# A Customizable 3D-Printable Stage Clamp

This is one of the [possible clamp designs](renders/README.md) that can be achieved with this OpenSCAD code:

![rotating animation of a rendered clamp](renders/50mm-Clamp-with-Hex-Screw-Hole.gif)

# [👉 Click HERE for more fancy animated renders 👈](renders/README.md)

## Usage

Open the `*.scad`-file in [OpenSCAD](https://openscad.org/) and customize the clamp to your needs using the Customizer. Press F6 to render, then F7 to export the STL file.

### OpenSCAD

This design was tested with **OpenSCAD 2021.01**. At the time of writing, the development snapshot has no support for unconstrained vectors in the Customizer, so certain features like the attachment hole don't work.

## 🖨 Hints for 3D Printing

To position the individual parts for 3D printing, set the parameter `positioning` (under `👁 Display`) to `🖨 printable`. This will place all components in a 3D-printer-friendly position.

The default parameters are optimized for 3D printing:

- **print-in-place** hinge, no assembly required, just putting the screws where they belong
- **no bridging** is necessary
- **no support** is necessary
- **no too-steep overhangs**

I have had good results with the following settings (on a Prusa i3 MK3S+ at least):

- PLA or PETG
- 0.4mm nozzle
- max. 0.3mm layer height
- 5-10% infill
- 100% infill for all screws
  - For the clamp screw it is enough to print the screw thread with 100% infill and about 1mm of the top half-cylindrical base, not all of the base
  - The attachment bolt **must** be printed completely with 100% infill, otherwise the head snaps off too easily.
- at least 2 perimeters

This results in average costs between 50ct and 1€ per clamp, an **order of magnitude** cheaper than aluminum ones.

By ramping up the speeds and using 1mm extrusion width for the first layer, the print time can be decreased to slightly above than 1 hour.

## 🗜️ ZIP creation

e.g. for https://printables.com

```bash
begin;openscad -d /dev/stdout stage-clamp.scad --export-format png -o /dev/null -q | string replace -f -r '^\t(.*?)(?:\s*\\\s*)?$' '$1' | string replace -r "^$(pwd)/" '';for f in **/LICENSE* *.json;echo "$f";end;end | xargs -d'\n' -t zip -r stage-clamp-(git describe --always --dirty).zip
```
