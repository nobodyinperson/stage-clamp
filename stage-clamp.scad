// clang-format off
include <utils/utils.scad>;
use <threads-scad/threads.scad>;
// clang-format on

/* [👁 Display] */
show_rod = true;
// render some operations to get rid of glitches
render_some = false;
rod_diameter = 49;
positioning = "🔩 assembled"; // ["🔩 assembled","🖨 printable"]
clamp_open_angle = 110;      // [0:1:190]
// -1 = max. inwards, 1 = max. outwards
clamp_screw_rotation = 1;       // [-1:0.01:1]
clamp_screw_nut_turns = 8;      // [0:0.05:20]
attachment_screw_nut_turns = 8; // [0:0.05:20]

/* [🔧 Clamp] */
clamp_inner_diameter = 50;
clamp_thickness = 7;
clamp_height = 40;
// clearance used in various different places
clamp_clearance = 0.5;
// edge radius used in different places
clamp_edge_radius = 2; // [0:0.1:10]
clamp_outer_diameter = clamp_inner_diameter + 2 * clamp_thickness;

clamp_loose_opening_angle = 130;
clamp_fixed_opening_angle = 160;
// How much to reduce the effective inner clamp diameter with the loose clamp
clamp_loose_reduce = 5; // [0:0.1:20]

/* [🔧 Clamp Hinge] */
clamp_hinge_thickness = 3;    // [1:0.1:10]
clamp_hinge_rod_diameter = 6; // [1:0.1:20]
clamp_hinge_rod_clearance = 0.2;
clamp_hinge_diameter = clamp_hinge_thickness * 2 + clamp_hinge_rod_diameter +
                       2 * clamp_hinge_rod_clearance;
clamp_offset = [
  clamp_hinge_diameter / 2 > clamp_thickness
    ? (clamp_hinge_diameter / 2 - clamp_thickness)
    : 0,
  0,
  0
];
clamp_hinge_outer_diameter = clamp_hinge_rod_diameter +
                             2 * clamp_hinge_rod_clearance +
                             2 * clamp_hinge_thickness;
clamp_hinge_segments = 1;          // [1:1:10]
clamp_hinge_segment_clearance = 9; // [0:0.01:50]
clamp_hinge_segment_height =
  clamp_height / clamp_hinge_segments - clamp_hinge_segment_clearance;
clamp_hinge_attachment_clearance = -10; // [-50:0.01:50]
clamp_hinge_attachment_offset = -2;     // [-50:0.1:50]

/* [🔩 Clamp Screw] */
clamp_screw_debug = false;
clamp_screw_base_diameter = 10;
clamp_screw_position = [ 0.33, 0.5 ]; // [0:0.01:1]
clamp_screw_pitch = 2;
clamp_screw_tooth_angle = 45;
clamp_screw_diameter = 8;
clamp_screw_length = 40;
clamp_screw_holder_width = 20;
clamp_screw_holder_thickness = 15;            // [1:0.1:50]
clamp_screw_holder_overhang_angle = 70;       // [30:1:90]
clamp_screw_max_movement_angle_inwards = 10;  // [0:1:90]
clamp_screw_max_movement_angle_outwards = 45; // [0:1:90]
clamp_nut_holder_width = 20;
clamp_nut_holder_thickness = 10;
clamp_nut_opening_size = 15;
clamp_nut_groove_depth = 2;

clamp_screw_nut_shape = "💸 wing"; // ["🍯 hex","💸 wing"]
clamp_screw_nut_diameter = 20;
clamp_screw_nut_height = 10;
// [diameter1, diameter2, flat_height]
clamp_screw_nut_wing_sizes = [ 11, 25, 5 ]; // [0:0.1:100]
clamp_screw_nut_wing_thread_inside = false;

/* [➕ Attachment] */
// clang-format off

attachment_type = "🚫 none"; // ["🚫 none","⬛ plate","➰ loop","🔻 slot","🔺 plug","🐊 croc"]

// clang-format on

attachment_depth = 5;              // [0:0.1:50]
attachment_width = 25;             // [1:0.1:50]
attachment_height = 40;            // [1:0.1:50]
attachment_transition_height = 40; // [0:0.1:50]
attachment_transition_height_capped =
  min(clamp_height, attachment_transition_height);
// Attachment rotation [X, Y, Z]
attachment_rotation = [ 0, 0, 0 ]; // [-90:0.1:90]
// Attachment offset [X, Y, Z]
attachment_offset = [ 0, 0, 0 ];        // [-50:0.1:50]
attachment_slot_depth = 0;              // [-10:0.01:10]
attachment_extra_inner_diameter = 20;   // [0:0.1:100]
attachment_extra_outer_diameter = 10.5; // [0:0.1:100]
attachment_clipped_to_clamp_height_top = false;
attachment_clipped_to_clamp_height_bottom = false;

/* [⚫ Hole] */
attachment_hole = true;
attachment_hole_debug = false;
// relative to attachment
attachment_hole_offset = [ 0, 0, 0 ]; // [-50:0.1:50]

// clang-format off

// sequence of cylinders [diameter1,length1,edges1,rotation1,diameter2,length2,edges2,rotation2,...]
attachment_hole_shape= [];   // [0:0.01:100]
// clang-format on
// make it a nested vector afterwards due to OpenSCAD issue #3817
attachment_hole_shape_ = groupn(attachment_hole_shape, 4);

/* [🔩 Attachment Screw] */
attachment_screw = false;
attachment_screw_debug = false;
attachment_screw_rotation = 0; // [-180:0.1:180]
attachment_screw_thread_diameter = 10;
attachment_screw_thread_pitch = 2;
attachment_screw_thread_tooth_angle = 45;
attachment_screw_thread_length = 30;
attachment_screw_thread_tip_height = 3;
attachment_screw_thread_tip_min_fract = 0.5;
attachment_screw_nut = true;
attachment_screw_nut_shape = "🍯 hex"; // ["🍯 hex","🔧 bar"]
attachment_screw_nut_diameter = 25;
attachment_screw_nut_height = 10;
attachment_screw_head_shape = "🍯 hex"; // ["🍯 hex","🔧 bar"]
attachment_screw_head_height = 5;
attachment_screw_head_diameter = 19;
attachment_screw_washer = true;
attachment_screw_washer_diameter = 25;
attachment_screw_washer_thickness = 2;

attachment_screw_assembled_offset = [ 0, 0, 0 ]; // [-100:0.1:100]

// 📏 Precision
$fs = $preview ? 3 : 0.5;
$fa = $preview ? 3 : 1;
epsilon = 1 * 0.1;

module
rounded_cube(size = [ 1, 1, 1 ], radius = 0.1)
{
  linear_extrude(size[2]) offset(radius) offset(-radius)
    square([ size[0], size[1] ]);
}

module
pipe_part(angle = 180,
          symmetricTo = false,
          outer_diameter = clamp_outer_diameter,
          thickness = clamp_thickness,
          height = clamp_height,
          edge_radius = clamp_edge_radius)
{
  rotate([ 0, 0, is_bool(symmetricTo) ? 0 : (symmetricTo * 2 - angle) / 2 ])
    rotate_extrude(angle = angle)
  {
    translate([ outer_diameter / 2 - thickness, 0, 0 ])
      offset(clamp_edge_radius) offset(-clamp_edge_radius)
        square([ thickness, height ]);
  }
}

module
clamp_screw_thread()
{
  ScrewThread(outer_diam = clamp_screw_diameter,
              height = clamp_screw_length,
              pitch = clamp_screw_pitch,
              tooth_angle = clamp_screw_tooth_angle,
              tip_height = 3,
              tip_min_fract = 0.5);
}

module
clamp_screw()
{
  module clamp_screw_base(length = clamp_height)
  {
    translate([ 0, 0, -clamp_screw_base_diameter / 2 ]) rotate([ 90, 0, -90 ])
      wedge(
        angle = 180, radius = clamp_screw_base_diameter / 2, height = length);
  }
  local_xyz(show = clamp_screw_debug)
    translate([ 0, 0, clamp_screw_base_diameter / 2 ])
  {
    translate([ clamp_height * clamp_screw_position[1], 0, 0 ])
      clamp_screw_base();
    clamp_screw_thread();
    // transition from base to screw
    hull()
    {
      translate([ 0, 0, -epsilon ]) linear_extrude(epsilon)
        projection(cut = true) clamp_screw_thread();
      translate([ clamp_screw_diameter / 2, 0, 0 ])
        clamp_screw_base(length = clamp_screw_diameter);
    }
  }
}

module
clamp_nut()
{
  module wing_shape()
  {
    distance =
      abs(clamp_screw_nut_wing_sizes[1] - clamp_screw_nut_wing_sizes[0]) / 2;
    hull() at([ -distance, distance ], "x") circle(
      d = min(clamp_screw_nut_wing_sizes[0], clamp_screw_nut_wing_sizes[1]));
  }
  render_if(render_some) ScrewHole(outer_diam = clamp_screw_diameter,
                                   height = clamp_screw_nut_height + epsilon,
                                   pitch = clamp_screw_pitch,
                                   tooth_angle = clamp_screw_tooth_angle)
  {
    if (clamp_screw_nut_shape == "🍯 hex")
      cylinder(
        d = clamp_screw_nut_diameter, h = clamp_screw_nut_height, $fn = 6);
    else if (clamp_screw_nut_shape == "💸 wing") {
      round_height = clamp_nut_groove_depth * 1.5;
      transition_height =
        clamp_screw_nut_height - round_height - clamp_screw_nut_wing_sizes[2];
      cylinder(d = clamp_screw_nut_diameter, h = round_height);
      // transition from round to wing
      hull()
      {
        // top of the round part
        translate([ 0, 0, round_height - epsilon ])
          cylinder(d = clamp_screw_nut_diameter, h = epsilon);
        // bottom of the wings
        translate([ 0, 0, round_height + transition_height - epsilon ])
          linear_extrude(epsilon) wing_shape();
      }
      // the wing part
      translate([ 0, 0, round_height + transition_height ])
        linear_extrude(clamp_screw_nut_wing_sizes[2]) difference()
      {
        wing_shape();
        if (!clamp_screw_nut_wing_thread_inside)
          circle(d = clamp_screw_diameter + 2 * clamp_clearance);
      }
    }
  }
}

module
attachment_screw()
{
  local_xyz(show = attachment_screw_debug)
    rotate([ 0, 0, attachment_screw_rotation ])
  {
    if (attachment_screw_head_shape == "🔧 bar") {
      linear_extrude(attachment_screw_head_height) hull()
      {
        at(
          [
            -attachment_screw_head_diameter / 4,
            attachment_screw_head_diameter / 4
          ],
          "x") circle(d = attachment_screw_head_diameter / 2);
      }
    } else {
      cylinder(d = attachment_screw_head_diameter,
               h = attachment_screw_head_height,
               $fn = 6);
    }
    translate([ 0, 0, attachment_screw_head_height ])
      ScrewThread(outer_diam = attachment_screw_thread_diameter,
                  height = attachment_screw_thread_length,
                  pitch = attachment_screw_thread_pitch,
                  tooth_angle = attachment_screw_thread_tooth_angle,
                  tip_height = attachment_screw_thread_tip_height,
                  tip_min_fract = attachment_screw_thread_tip_min_fract);
  }
}

module
attachment_screw_washer()
{
  local_xyz(show = attachment_screw_debug) difference()
  {
    cylinder(d = attachment_screw_washer_diameter,
             h = attachment_screw_washer_thickness);
    translate([ 0, 0, -epsilon / 2 ])
      cylinder(d = attachment_screw_thread_diameter + 2 * clamp_clearance,
               h = attachment_screw_washer_thickness + epsilon);
  }
}

module
attachment_screw_nut()
{
  local_xyz(show = attachment_screw_debug)
    rotate([ 0, 0, attachment_screw_rotation ])
      ScrewHole(outer_diam = attachment_screw_thread_diameter,
                height = attachment_screw_nut_height,
                pitch = attachment_screw_thread_pitch,
                tooth_angle = attachment_screw_thread_tooth_angle)
  {
    if (attachment_screw_nut_shape == "🔧 bar") {
      linear_extrude(attachment_screw_nut_height) hull()
      {
        at(
          [
            -attachment_screw_nut_diameter / 4,
            attachment_screw_nut_diameter / 4
          ],
          "x") circle(d = attachment_screw_nut_diameter / 2);
      }
    } else {
      cylinder(d = attachment_screw_nut_diameter,
               h = attachment_screw_nut_height,
               $fn = 6);
    }
  }
}

module
attach_hinge(diameter,
             length,
             n,
             outer = true,
             segment_clearance = 0.4,
             center = false,
             rod_diameter = undef,
             rod_clearance = 0.4,
             attachment_clearance = 4,
             attachment_offset = 0,
             max_distance = undef, )
{
  max_distance = is_undef(max_distance) ? 10 * (diameter + 1) : max_distance;
  rod_diameter = is_undef(rod_diameter) ? diameter / 2 : rod_diameter;
  segment_space = length / (2 * n);
  segment_height = length / (2 * n) - segment_clearance;

  module segment(i, diameter = diameter, segment_height = segment_height)
  {

    translate([
      0,
      0,
      i * segment_space -
        ((outer && abs(i) == n) ? sign(i) * segment_height / 2 / 2 : 0)
    ])
      cylinder(d = diameter,
               h = (outer && abs(i) == n) ? segment_height / 2 : segment_height,
               center = true);
  }

  translate([ 0, 0, center ? 0 : length / 2 ]) difference()
  {
    union()
    {
      // shaft rod through the outer segments
      if (outer)
        cylinder(d = rod_diameter, h = length, center = true);

      for (i = outer ? [-n:2:n] : [-n + 1:2:n - 1]) {
        // attachment: transition to children
        hull()
        {
          intersection()
          {
            // undo centering for children if necessary
            translate([ 0, 0, (center ? 0 : -length / 2) ]) children();
            union()
            {
              translate([ 0, 0, attachment_offset ])
                segment(i,
                        diameter = max_distance,
                        segment_height = segment_space - attachment_clearance);
              segment(
                i, diameter = max_distance, segment_height = segment_height);
            }
          }
          segment(i);
        }
      }
    }
    // cutout through the inner segments
    if (!outer)
      cylinder(d = rod_diameter + 2 * rod_clearance, h = length, center = true);
  }
}

/////////////////////
/// 🔧 The clamps ///
/////////////////////

/// 🔧 The loose part ///
rotate([ 0, 0, 180 - clamp_open_angle ]) render_if(render_some) difference()
{
  // clamp, hinge and nut holder solid parts
  union()
  {
    // the clamp part
    translate([ -clamp_outer_diameter / 2, -clamp_loose_reduce, 0 ])
      translate(-clamp_offset)
        pipe_part(angle = clamp_loose_opening_angle, symmetricTo = 90);

    // the hinge
    attach_hinge(outer = true,
                 diameter = clamp_hinge_outer_diameter,
                 length = clamp_height,
                 n = clamp_hinge_segments,
                 segment_clearance = clamp_hinge_segment_clearance,
                 attachment_clearance = clamp_hinge_attachment_clearance,
                 attachment_offset = clamp_hinge_attachment_offset)
      translate([ -clamp_outer_diameter / 2, -clamp_loose_reduce, 0 ]) hull()
    {
      // a tiny bit of the loose clamp part
      translate(-clamp_offset) intersection()
      {
        wedge(angle = (180 - clamp_loose_opening_angle) / 2 + epsilon,
              height = clamp_height,
              radius = clamp_outer_diameter / 2 + epsilon);
        pipe_part(angle = clamp_loose_opening_angle, symmetricTo = 90);
      }
    }

    // nut holder
    translate([ 0, -clamp_loose_reduce, 0 ]) translate([
      -cos((180 - clamp_loose_opening_angle) / 2) * clamp_inner_diameter / 2,
      sin((180 - clamp_loose_opening_angle) / 2) * clamp_inner_diameter / 2,
      0
    ]) translate([ -clamp_outer_diameter / 2, 0, 0 ])
      translate([ -(clamp_nut_holder_width + clamp_thickness), 0, 0 ])
        translate(-clamp_offset) render_if(render_some) difference()
    {
      rounded_cube(
        [
          clamp_nut_holder_width + clamp_thickness,
          clamp_nut_holder_thickness,
          clamp_height
        ],
        radius = clamp_edge_radius);
    }
  }
  // remove parts from loose clamp
  translate([ 0, -clamp_loose_reduce, 0 ]) translate([
    -cos((180 - clamp_loose_opening_angle) / 2) * clamp_inner_diameter / 2,
    sin((180 - clamp_loose_opening_angle) / 2) * clamp_inner_diameter / 2,
    0
  ]) translate([ -clamp_outer_diameter / 2, 0, 0 ])
    translate([ -(clamp_nut_holder_width + clamp_thickness), 0, 0 ])
      translate(-clamp_offset) render_if(render_some)
  {
    // groove for the nut
    translate([
      clamp_screw_holder_width * (1 - clamp_screw_position[0]) -
        (clamp_screw_holder_width - clamp_nut_holder_width),
      clamp_nut_holder_thickness + epsilon,
      clamp_height * clamp_screw_position[1]
    ]) rotate([ 90, 0, 0 ])
      cylinder(d2 = clamp_screw_nut_diameter + clamp_clearance,
               d1 = clamp_screw_nut_diameter + 2 * clamp_clearance,
               h = clamp_nut_groove_depth + epsilon);
    // insert slot for screw
    hull()
    {
      translate([
        -epsilon / 2,
        (clamp_nut_holder_thickness + epsilon) / 2,
        clamp_height * clamp_screw_position[1] +
          (clamp_nut_opening_size -
           (clamp_screw_diameter + 2 * clamp_clearance)) /
            2
      ]) rotate([ -90, 0, 0 ])
        cube(
          [
            epsilon,
            clamp_nut_opening_size,
            clamp_nut_holder_thickness +
            epsilon
          ],
          center = true);
      translate([
        clamp_screw_holder_width * (1 - clamp_screw_position[0]) -
          (clamp_screw_holder_width - clamp_nut_holder_width),
        0,
        clamp_height * clamp_screw_position[1]
      ]) rotate([ -90, 0, 0 ]) translate([ 0, 0, -epsilon / 2 ])
        cylinder(d = clamp_screw_diameter + 2 * clamp_clearance,
                 h = clamp_nut_holder_thickness + epsilon);
    }
  }
}

module
move_to_assembled_screw_position()
{
  translate(clamp_offset) translate([
    clamp_outer_diameter + clamp_screw_holder_width * clamp_screw_position[0],
    clamp_thickness + clamp_screw_base_diameter / 2,
    clamp_height * clamp_screw_position[1]
  ]) rotate([ 90, 90, 0 ]) children();
}

module
clamp_screw_shape(offset = clamp_clearance)
{
  offset(offset) projection() clamp_screw_thread();
}

module
fixed_part_attachment()
{
  if (attachment_type == "➰ loop") {
    loop_thickness =
      (attachment_extra_outer_diameter - attachment_extra_inner_diameter) / 2;
    translate([ 0, 0, attachment_extra_outer_diameter / 2 ])
      rotate([ 90, 0, 0 ]) rotate_extrude() translate(
        [ attachment_extra_inner_diameter / 2 + loop_thickness / 2, 0 ])
        offset(clamp_edge_radius) offset(-clamp_edge_radius)
          square([ loop_thickness, attachment_height ], center = true);
  } else if (attachment_type != "🚫 none") {
    translate([ 0, 0, attachment_depth / 2 ]) cube(
      [
        attachment_width,
        min(attachment_height, clamp_height),
        attachment_type == "⬛ plate" ? max(attachment_depth, epsilon)
                                     : attachment_depth
      ],
      center = true);
  }
}

module
move_to_attachment_position()
{
  translate(
    [ clamp_outer_diameter / 2, clamp_outer_diameter / 2, clamp_height / 2 ])
    translate(attachment_offset) translate(clamp_offset)
      rotate(attachment_rotation) rotate([ -90, 0, 0 ]) children();
}

module
slot_shape()
{
  wide = max(attachment_extra_inner_diameter, attachment_extra_outer_diameter);
  narrow =
    min(attachment_extra_inner_diameter, attachment_extra_outer_diameter);
  polygon([
    [ -narrow / 2, 0 ],
    [ -wide / 2, attachment_slot_depth ],
    [ wide / 2, attachment_slot_depth ],
    [ narrow / 2, 0 ],
  ]);
}

/// 🔧 The fixed part ///
render_if(render_some) difference()
{
  union()
  {
    render_if(attachment_type == "🔺 plug", convexity = 5) difference()
    {
      union()
      {
        translate([ clamp_outer_diameter / 2, 0, 0 ]) translate(clamp_offset)
          pipe_part(angle = clamp_fixed_opening_angle);

        /////////////////////
        /// ➕ Attachment ///
        /////////////////////
        if (attachment_type != "🚫 none" && attachment_type != "🐊 croc") {
          // transition to attachment
          difference()
          {
            // „transition” including the complete attachment shape and
            // filling part of the clamp inners
            intersection()
            {
              hull()
              {
                move_to_attachment_position() fixed_part_attachment();
                translate([
                  clamp_outer_diameter / 2,
                  0,
                  -attachment_transition_height_capped / 2 + clamp_height / 2 +
                    sign(attachment_offset[2]) *
                      min((clamp_height - attachment_transition_height_capped) /
                            2,
                          abs(attachment_offset[2]))
                ]) translate(clamp_offset)
                  pipe_part(angle = clamp_fixed_opening_angle,
                            height = attachment_transition_height_capped);
              }
              if (any([
                    attachment_clipped_to_clamp_height_top,
                    attachment_clipped_to_clamp_height_bottom
                  ])) {
                width =
                  attachment_width + 2 * abs(attachment_offset[0]) + epsilon;
                depth = clamp_outer_diameter / 2 + attachment_depth +
                        2 * abs(attachment_offset[1]) + epsilon;
                height =
                  clamp_height + abs(attachment_offset[2]) + attachment_height;
                translate([
                  clamp_outer_diameter / 2,
                  clamp_outer_diameter / 2,
                  clamp_height / 2
                ]) intersection()
                {
                  if (attachment_clipped_to_clamp_height_top)
                    translate([ 0, 0, -height / 2 + clamp_height / 2 ])
                      cube([ width, depth, height ], center = true);
                  if (attachment_clipped_to_clamp_height_bottom)
                    translate([ 0, 0, +height / 2 - clamp_height / 2 ])
                      cube([ width, depth, height ], center = true);
                }
              }
            }
            // remove the stuff blocking the inners of the clamp
            cutout_height =
              clamp_height + 2 * abs(attachment_offset[2]) + epsilon;
            translate([ clamp_outer_diameter / 2, 0, 0 ])
              translate(clamp_offset) translate([ 0, 0, clamp_height / 2 ])
                cylinder(d = clamp_outer_diameter - 2 * clamp_thickness +
                             clamp_thickness,
                         h = cutout_height,
                         center = true);
          }
        }
      }
    }

    attach_hinge(outer = false,
                 diameter = clamp_hinge_outer_diameter,
                 length = clamp_height,
                 n = clamp_hinge_segments,
                 segment_clearance = clamp_hinge_segment_clearance,
                 attachment_clearance = clamp_hinge_attachment_clearance,
                 attachment_offset = clamp_hinge_attachment_offset)
      translate([ clamp_outer_diameter / 2, 0, 0 ]) hull()
    {
      // a tiny bit of the fixed clamp part
      translate(clamp_offset) intersection()
      {
        rotate([ 0, 0, clamp_fixed_opening_angle - epsilon ])
          wedge(angle = (180 - clamp_fixed_opening_angle) + epsilon,
                height = clamp_height,
                radius = clamp_outer_diameter / 2 + epsilon);
        pipe_part(angle = clamp_fixed_opening_angle);
      }
    }
    // screw holder
    difference()
    {
      // base shape for holder
      translate([ clamp_outer_diameter - clamp_thickness, 0, 0 ])
        translate(clamp_offset) rounded_cube(
          [
            clamp_screw_holder_width + clamp_thickness,
            clamp_screw_holder_thickness,
            clamp_height,
          ],
          radius = clamp_edge_radius);

      // groove for the screw base
      move_to_assembled_screw_position() hull()
      {
        rotate([ 0, 90, 0 ]) at([ 0, clamp_screw_holder_thickness ], "x")
          translate([
            0,
            0,
            -clamp_height * (1 - clamp_screw_position[1]) - epsilon / 2
          ]) cylinder(d = clamp_screw_base_diameter + clamp_clearance,
                      h = clamp_height + epsilon);
      }
    }
  }
  // hole for the screw
  move_to_assembled_screw_position() hull()
  {
    for (angle = [
           clamp_screw_max_movement_angle_inwards,
           -clamp_screw_max_movement_angle_outwards
         ]) {
      // left and right extreme
      rotate([ angle, 0, 0 ]) linear_extrude(clamp_screw_length)
        clamp_screw_shape();
      rotate([ -clamp_screw_max_movement_angle_outwards, 0, 0 ])
        linear_extrude(clamp_screw_length) clamp_screw_shape();
      // length extreme
      translate([ 0, 0, clamp_screw_length ]) rotate([ sign(angle) * 90, 0, 0 ])
        linear_extrude(max(sign(angle) * sin(angle) * clamp_screw_length,
                           epsilon)) clamp_screw_shape();
    }
    // upward overhang extreme
    translate([
      -((clamp_screw_diameter / 2 + clamp_clearance) +
        tan(90 - clamp_screw_holder_overhang_angle) *
          (clamp_thickness + clamp_screw_base_diameter / 2)),
      0,
      clamp_thickness + clamp_screw_base_diameter / 2
    ]) translate([ 0, 0, epsilon / 2 ])
      cube(
        [
          epsilon,
          sin(clamp_screw_max_movement_angle_outwards) *
            clamp_screw_holder_width,
          epsilon
        ],
        center = true);
  }
  //////////////////////////
  /// ⚫ Attachment Hole ///
  //////////////////////////
  if (attachment_hole)
    highlight_if(attachment_hole_debug) move_to_attachment_position()
      translate(attachment_hole_offset)
        cylinder_sequence(attachment_hole_shape_);
  /////////////////////
  /// ➕ Attachment ///
  /////////////////////
  // hole through the loop
  move_to_attachment_position() if (attachment_type == "➰ loop")
  {
    cutout_height = attachment_height + clamp_height +
                    2 * abs(attachment_offset[2]) + epsilon;
    translate([ 0, 0, attachment_extra_outer_diameter / 2 ])
      rotate([ 90, 0, 0 ]) cylinder(
        d = attachment_extra_inner_diameter, h = cutout_height, center = true);
  }
  // remove the slot from the attachment
  else if (attachment_type == "🔻 slot")
  {
    translate([ 0, 0, attachment_depth ]) rotate([ 90, 180, 0 ]) linear_extrude(
      clamp_height + attachment_height + abs(attachment_offset[2]) + epsilon,
      center = true) offset(clamp_clearance / 2) slot_shape();
  }
  // remove stuff around the plug from the attachment
  else if (attachment_type == "🔺 plug")
  {
    translate([ 0, 0, -attachment_slot_depth + attachment_depth ])
      rotate([ 90, 0, 0 ]) linear_extrude(clamp_height + attachment_height +
                                            abs(attachment_offset[2]) + epsilon,
                                          center = true) difference()
    {
      width = max([
        attachment_extra_outer_diameter,
        attachment_extra_inner_diameter,
        clamp_outer_diameter
      ]);
      translate([ -(width + epsilon) / 2, 0 ]) square([
        width + epsilon,
        max(attachment_depth, attachment_slot_depth) +
        epsilon
      ]);
      slot_shape();
    }
  }
}

// what the counterpart would look like
// if (attachment_type == "🔻 slot") {
//   move_to_attachment_position() translate([ 0, 0, attachment_depth ])
//     rotate([ 90, 180, 0 ]) linear_extrude(attachment_height, center = true)
//       slot_shape();
// }

///////////////////////////
/// 🔩 Attachment Screw ///
///////////////////////////
debug_if(attachment_screw_debug) if (attachment_screw)
{
  if (positioning == "🖨 printable") {
    translate([
      clamp_outer_diameter + clamp_screw_holder_width +
        clamp_screw_nut_diameter,
      clamp_height * 1.5
    ])
    {
      attachment_screw();
      if (attachment_screw_nut)
        translate([ attachment_screw_head_diameter * 1.5, 0, 0 ])
          attachment_screw_nut();
      if (attachment_screw_washer)
        translate([
          attachment_screw_head_diameter * 1.5 +
            attachment_screw_nut_diameter * 1.5,
          0,
          0
        ]) attachment_screw_washer();
    }
  } else {
    move_to_attachment_position() translate(attachment_screw_assembled_offset)
      translate([ 0, 0, -attachment_depth ])

    {
      attachment_screw();
      translate([
        0,
        0,
        attachment_screw_head_height + attachment_screw_nut_turns *
        attachment_screw_thread_pitch
      ]) rotate([ 0, 0, 360 * attachment_screw_nut_turns ])
      {
        if (attachment_screw_washer)
          translate(
            [ 0, 0, -attachment_screw_washer_thickness - clamp_clearance ])
            attachment_screw_washer();
        if (attachment_screw_nut)
          attachment_screw_nut();
      }
    }
  }
}

///////////////////////
/// 🔩 Bolt and Nut ///
///////////////////////
if (positioning == "🖨 printable") {
  translate([
    clamp_outer_diameter + clamp_screw_holder_width + clamp_screw_nut_diameter,
    0,
    0
  ]) rotate([ 0, 0, 90 ])
  {
    clamp_screw();
    translate([ 0, -(clamp_screw_base_diameter + clamp_screw_nut_diameter), 0 ])
      clamp_nut();
  }
} else {
  move_to_assembled_screw_position()
  {
    rotate([
      -(clamp_screw_rotation < 0 ? clamp_screw_max_movement_angle_inwards
                                 : clamp_screw_max_movement_angle_outwards) *
        clamp_screw_rotation,
      0,
      0
    ])
    {
      clamp_screw();
      translate([
        0,
        0,
        clamp_screw_base_diameter + clamp_screw_pitch *
        clamp_screw_nut_turns
      ]) rotate([ 0, 0, 360 * clamp_screw_nut_turns ]) clamp_nut();
    }
  }
}

// the rod
if (show_rod && $preview)
  translate([
    clamp_outer_diameter / 2,
    (clamp_inner_diameter - rod_diameter) / 2,
    -clamp_height / 2
  ]) translate(clamp_offset) color("black", 0.2)
    cylinder(d = rod_diameter, h = clamp_height * 2);
