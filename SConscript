import re
import itertools


Import("env scadfiles")
env["RENDERS_DIR"] = "renders"
env["EXPORTS_DIR"] = "exports"


def sanitize(s):
    return re.sub(r"-+", r"-", re.sub(r"[^\w-]", r"-", s))


renderfiles = dict()
stlfiles = dict()
renderanimations = dict()
xvfb_run_server_nr = itertools.count(100)
for scadfile in scadfiles:
    for parameterFile, parameterSet in (
        (env["openscad_get_parameter_file"](scadfile), ps)
        for ps in env["openscad_get_parameter_sets"](scadfile)
    ):
        subenv = env.Clone(
            OPENSCAD_FILE=scadfile,
            OPENSCAD_PARAMETER_FILE=parameterFile,
            OPENSCAD_PARAMETER_SET=parameterSet,
            OPENSCAD_PARAMETER_SET_SANITIZED=sanitize(parameterSet),
        )
        renderfiles[parameterSet] = []
        for viewangle in range(0, 360, 5):
            renderfiles[parameterSet].append(
                subenv.Render(
                    "$RENDERS_DIR/$OPENSCAD_FILE-$GIT_COMMIT_DESCRIPTION-${OPENSCAD_PARAMETER_SET_SANITIZED}-camera-${CAMERA_ANGLE}.png",
                    "$OPENSCAD_FILE",
                    OPENSCAD_FLAGS="-Dshow_rod=true "
                    "--viewall --autocenter --projection=perspective --camera=0,0,0,45,0,$CAMERA_ANGLE,1",
                    XVFB_RUN_FLAGS=f"-a -n{next(xvfb_run_server_nr)}",
                    CAMERA_ANGLE=viewangle,
                )
            )
        subenv.Command(
            "$RENDERS_DIR/$OPENSCAD_FILE-$GIT_COMMIT_DESCRIPTION-${OPENSCAD_PARAMETER_SET_SANITIZED}.gif",
            renderfiles[parameterSet],
            "magick -dispose previous $SOURCES -transparent '#ffffe5' -delay 100 $TARGET",
        )
        # webp but not as well supported as GIF
        # subenv.Command(
        #     "$RENDERS_DIR/$OPENSCAD_FILE-$GIT_COMMIT_DESCRIPTION-${OPENSCAD_PARAMETER_SET_SANITIZED}.webp",
        #     renderfiles[parameterSet],
        #     "magick -dispose previous $SOURCES -transparent '#ffffe5' -set delay 10 webp:$TARGET",
        # )
        renderanimations[parameterSet] = subenv.Command(
            "$RENDERS_DIR/${OPENSCAD_PARAMETER_SET_SANITIZED}.gif",
            "$RENDERS_DIR/$OPENSCAD_FILE-$GIT_COMMIT_DESCRIPTION-${OPENSCAD_PARAMETER_SET_SANITIZED}.gif",
            Copy("$TARGET", "$SOURCE"),
        )
        stlfiles[parameterSet] = subenv.STL(
            "$EXPORTS_DIR/$OPENSCAD_FILE-$GIT_COMMIT_DESCRIPTION-${OPENSCAD_PARAMETER_SET_SANITIZED}.stl",
            "$OPENSCAD_FILE",
            OPENSCAD_FLAGS="""-Dpositioning='"🖨 printable"'""",
        )
        stlfiles[parameterSet] = subenv.Symlink(
            "$EXPORTS_DIR/$OPENSCAD_FILE-${OPENSCAD_PARAMETER_SET_SANITIZED}.stl",
            "$EXPORTS_DIR/$OPENSCAD_FILE-$GIT_COMMIT_DESCRIPTION-${OPENSCAD_PARAMETER_SET_SANITIZED}.stl",
            SYMLINK_RELATIVE=True,
        )
        subenv.Default(stlfiles[parameterSet])

renders_readme = env.Textfile(
    "$RENDERS_DIR/README.md",
    source=[
        "# Rendered Parameter Sets",
        "These are the preconfigured parameter sets shipped with the design. "
        "You can select them in the Customizer.",
    ]
    + [
        f"# {k}\n\n![{k}]({str(''.join(map(str,v)))})"
        for k, v in renderanimations.items()
    ],
    LINESEPARATOR="\n\n",
)

env.Alias("animations")
env.Depends("animations", renders_readme)
env.Depends(
    "animations", list(itertools.chain.from_iterable(renderanimations.values()))
)

## Syncing PrusaSlicer configs
prusaenv = env.Clone(tools=["prusa-slicer"])
prusaenv.AddPrusaProfileSyncAlias(
    profile_dir_local="prusa-profiles", similar_names=scadfiles
)
